# PoGER-next

This is a continuation of the PoGER project, mainly focused on actually making a game engine.

Stated priorities :
 - Runs games
 - Flexibility in the architecture
 - OS-independence (multi-platform)
 - Performance
 - Division of responsabilities in the code
 - Code readability

## Story
I needed a custom tiled map engine to work with another project of mine
with (unfortunately) exotic tile format.

For the story, I am working on PoGER, a Pokemon Essentials project aiming
at extracting resources and re-implementing it on a open-source engine.

Code was inspired from :
- PyTMX : https://github.com/bitcraft/PyTMX
- kivy-tiled : https://github.com/PhoenixWright/kivy-tiled


MVC:
 - Model : Represents the game world. Receives controller's commands to 
           modify the model, pushes update to view.
 - View : Displays the game to the user. Is re-drawn when model updates it.
 - Controller : polls inputs, issues commands to model.