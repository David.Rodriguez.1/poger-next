################################################################ 
# 
# Author : DRS
# 
# Purpose : Resource management - Facilitates access to files
# 
################################################################ 


from pathlib import Path
from collections import namedtuple
import json
import logging

def JSONdecode( obj ):
    ''' Takes a dictionnary decoded from reading a JSON file and determines if it is
    an object instance. If so, it tries to return by calling the relevant init.

    :param obj: dict instance, typically an object representation
    :return: decoded object
    '''
    logging.info(f'JSONdecode: obj={obj}')

    c = obj.get('_class', False)
    if not isinstance( c, str ):
        # Not a class instance, just a dict object
        return obj
    del obj['_class']
    
    # Get the class to initialize object
    corresponding_class = eval(c)
    assert corresponding_class != None, f'JSONdecode: object with unexpected class identifier : {c}'
    logging.info(f"JSONdecode: obj is a {type(obj)}, mapping to {c}")
    # return object
    return corresponding_class( obj )

def read_JSON_file( file ):
    ''' Takes path-like `file` argument and opens it as a JSON file.
    The contents are passed to JSONdecode as a dict.

    :param file: path-like `Path` instance representing a file containing a JSON-encoded object
    :return: decoded object
    '''
    logging.info(f'read_JSON_file: file={file}')
    with file.open( 'r', encoding='utf8' ) as f:
        tmp = json.load( f, object_hook=JSONdecode )
    return tmp

def JSONencode( obj ):
    ''' Takes an object and returns a JSON-compatible representation (dict).
    
    :param obj: object for which we want a JSON-compatible representation
    :return: JSON-compatible representation of `obj`
    '''
    res = obj.__dict__
    res['_class'] = obj.__class__.__name__
    logging.info(f'JSONencode: res={res}')
    return res

def save_JSON_file( file, obj ):
    ''' Takes path-like `file` argument and opens it as a JSON file.
    Writes a JSON-compatible representation of given `obj` object obtained
    from JSONencode.

    :param file: path-like `Path` instance representing destination file
    :param obj: object instance to save
    '''
    logging.info(f'save_JSON_file: file={file}, obj={obj}')
    with file.open( 'w', encoding='utf8' ) as f:
        json.dump( JSONencode(obj), f, sort_keys=True, indent=4 )

class FileCollector:
    ''' Facilitates fetching files '''
    def __init__( self, root, debug=False ):
        self.debug = debug
        self.root = root

        if self.debug:
            logging.info("FileCollector: root={}".format( self.root ))

    def collect( self, pattern='**/*.*' ):
        ''' Uses pathlib's glob to find any file within `root` which can be matched
        by `pattern`. See `https://docs.python.org/3/library/pathlib.html#pathlib.Path.glob`

        :param pattern: pattern to match
        :return: list of Path objects representing paths matched files
        '''
        files = []

        if isinstance( pattern, list ):
            patterns = pattern
            assert 0 < len(patterns)
            for p in patterns:
                files.extend( self.collect(p) )
        else:
            # 11/11/2020 BUGFIX : was collecting files in trash like a cyber racoon
            files = [item.resolve() for item in self.root.glob( pattern ) if \
                item.is_file() and (not '$RECYCLE.BIN' in item.parts)]

            if self.debug:
                msg = "\tFound {} files in {} with pattern '{}'".format(len(files), self.root, pattern)
                print( msg )
                logging.info( msg )

        return files

class Config(dict):
    """ Meant to contain data needed at launch
    WARNING : No more than one instance should exist at any time for obvious reasons.
    """

    FILE = Path() / 'config.json'

    @staticmethod
    def bootstrap():
        ''' Reads configuration from data, returns Config instance

        :return: Config instance which content is taken from config file
        '''
        return read_JSON_file( Config.FILE )

    def save( self ):
        ''' Writes current configuration to config file
        '''
        save_JSON_file( Config.FILE, self )


class Resources( object ):
    """ Ressource manager
    Facilitates access to any relevant Poger ressource (file)
    """

    _TYPE = namedtuple( '_TYPE', ['path', 'filename_structure'] )
    
    GRAPHIC = _TYPE(Path("Graphics"),    ['**/{}.png','**/{}.PNG'])
    MAP     = _TYPE(Path("Maps"),        ['**/{}_*.json'])
    EVENT   = _TYPE(Path("Maps"),        ['**/{}_*.event'])
    TILESET = _TYPE(Path("Tileset_data"),['**/{}.json'])

    def __init__( self, verbose=False ):
        self.types = [ self.GRAPHIC, self.MAP, self.EVENT, self.TILESET ]
        self.verbose = verbose
        self.cfg = None

        # Data structure to minimise storage access by caching ressources
        type_paths = set([type.path for type in self.types])
        self.res = { type_path:dict() for type_path in type_paths }
        # Keep a dedicated FileCollector instance
        self.fc  = { type_path:FileCollector( type_path, debug=verbose )
                        for type_path in type_paths }

    def fetch( self, type, ident ):
        """ Returns a path-like object (pathlib.Path instance) for the 
        appropriate ressource.

        ex: Resources.fetch( Resources.MAP, 33 ) retrieves the file for map 33
        ex: Resources.fetch( Resources.EVENT, [33,4] ) retrieves the file for event 5 of map 33
        ex: Resources.fetch( Resources.EVENT, [33,'*'] ) retrieves the file for all events of map 33

        :param type: one of the following types : Ressource.<GRAPHIC,MAP,EVENT,TILESET>
        :param ident: file name identifier of the ressource to fetch
        :return: pathlib.Path instance for the corresponding ressource
        """
        assert type in self.types, f"Unknown type {type}. Accepted types : {self.types}"
        if isinstance(ident,list):
            # Ex : event 2 in map 33 with name "33_2_NPCJohn.event" has equivalent idents : [33,2] <=> "33_2"
            ident = '_'.join(ident)
        if isinstance(ident,int):
            ident = str(ident)

        logging.info( f"Fetching from '{type.path}' for '{ident}'" )
        # print( self.res )

        try:
            return self.res[type.path][ident]
        except KeyError:
            # first time fetching the ressource : using file collector
            # Bugfix : glob on Windows performs case-insensitive search. Fix : add set transform to keep unique elements
            #print(f"First time fetching")
            files = set(
                self.fc[ type.path ].collect( 
                    [ pattern.format(ident) for pattern in type.filename_structure ] ))

            if '*' in ident:
                # n results expected : return list
                res = files
            else:
                # Singular result expected
                assert len(files)==1, \
                    f'File collection error : looking for "{ident}" in {type.path} yielded : {files}'

                # store file's Path for later use and returns it
                res = files.pop()

            self.res[type.path][ident] = res

            return res

    def get_config( self ):
        if self.cfg is None:
            self.cfg = Config.bootstrap()

        return self.cfg




if __name__=='__main__':
    logging.basicConfig(filename='resources-debug.log', filemode='w', level=logging.DEBUG) #encoding='utf-8'
    rm = Resources( verbose=True )
    if True:
        # testing Config creation, saving and reloading
        overwrite_cfg = False

        if not overwrite_cfg and Config.FILE.is_file():
            cfg = rm.get_config()
            print(f"Retrieved cfg : {cfg}")
        else:
            from random import randint
            cfg = Config()
            cfg.screen_size = [randint(0,100), randint(0,100)]
            print(f'New Config : {cfg.__dict__}. Saving ..')
            cfg.save()
