import kivy
from kivy.core.window import Window
from kivy.clock import Clock

class Controller:

    valid_keys = ['up','right','down','left']

    def __init__( self, model, *args, **kwargs ):
        super().__init__( *args, **kwargs )
        self.model = model
        self._keyboard = Window.request_keyboard(self._keyboard_closed, self)
        self._keyboard.bind(on_key_down=self._on_keyboard_down)
        self._keyboard.bind(on_key_up=self._on_keyboard_up)

        self._pressed_keys = list()
        Clock.schedule_interval( self._poll, 1/30 )

    def _poll( self, dt, *args, **kwargs ):
        print(f'controller::poll: dt={dt}, args={args}, kwargs={kwargs}')
        

    def _keyboard_closed(self):
        self._keyboard.unbind(on_key_down=self._on_keyboard_down)
        self._keyboard = None

    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        print(f"_on_keyboard_down: keyboard={keyboard}, keycode={keycode}, text={text}, modifiers={modifiers}")
        # key = keycode[1]
        # if key in self.valid_keys:
        #     self.model.move_player( key )
        #     return True
        return False

    def _on_keyboard_up(self, keyboard, keycode, *args, *kwargs):
        print(f"_on_keyboard_up: keyboard={keyboard}, keycode={keycode}, text={text}, modifiers={modifiers}")
        return False