import json, sys
import numpy as np
from pathlib import Path
from Resources import Resources
import pandas as pd
from kivy.core.image import Image
from kivy.graphics.texture import Texture, TextureRegion
# to initialize OpenGL
from kivy.core.window import Window

TILESIZE=32
TILESET_ZERO=384
SUBTILESIZE=int(TILESIZE/2)
SUBTILE_BOX={'NW':(0, SUBTILESIZE, SUBTILESIZE, SUBTILESIZE),
            'NE':(SUBTILESIZE, SUBTILESIZE, SUBTILESIZE, SUBTILESIZE),
            'SE':(SUBTILESIZE, 0, SUBTILESIZE, SUBTILESIZE),
            'SW':(0, 0, SUBTILESIZE, SUBTILESIZE)}
SUBTILE_DIRECTIONS=['NW', 'NE', 'SE', 'SW']
SUBTILE_DECODER='AutotileDecode.csv'

class TilesetReader( object ):
    
    def __init__( self, source ):
        self._source = source
        print( f"Loading {source}" )
        self.texture = Image( str(source) ).texture
        
        # Trying fix for resize method
        self.texture.mag_filter = 'nearest'

        self.width, self.height = self.texture.size
        self.nbCellPerCol = None
        self.nbCellPerRow = None
        self.tiles = None
        
    def getTilefromCoordinates(self, tileX, tileY):
        """ returns the TextureRegion associated with passed indexes.
        Mostly used by getTilefromID.

        :param tileX,tileY: tile (x,y) indexes (from 0)
        :return: corresponding TextureRegion
        """
        # Note from doc : The origin of the image is at bottom left.
        x = (TILESIZE * tileX)
        y = self.height - TILESIZE * (tileY+1)
        return self.texture.get_region( x, y, TILESIZE, TILESIZE )
        
    def getTilefromID(self, tileID, frame=None):
        pass

    def __str__(self):
        return f'<{self.__class__.__name__}>: {self.nbCellPerCol}x{self.nbCellPerRow} cells.'
        


class PogerTilesetReader( TilesetReader ):
    def __init__( self, *args, **kwargs ):
        super( PogerTilesetReader, self ).__init__( *args, **kwargs )

        # Determining tileset format
        self.nbCellPerCol = int( self.height / TILESIZE )
        self.nbCellPerRow = int( self.width / TILESIZE )
        self.size = self.nbCellPerCol * self.nbCellPerRow
        self.tiles = dict() # tileID:TextureRegion dictionnary
        
        # Check if tileset respects format convention
        assert self.nbCellPerRow==8, \
            f"Format error : expected tileset to be 8 tiles wide, found {self.nbCellPerCol} in {self._source}."
        
    def getTilefromID(self, tileID, frame=None):
        """ returns the TextureRegion associated with tile ID.

        :param tileID: tile indexes (0)->(size)
        :return: corresponding TextureRegion
        """
        try:
            # If already cached, simply return tile texture
            return self.tiles[tileID]
        except KeyError:
            # If not, compute it ..
            (row, col) = divmod(tileID, self.nbCellPerRow)
            tile = self.getTilefromCoordinates(col,row)
            # .. then cache it and return it
            self.tiles[tileID] = tile
            return tile



class PogerAutotileReader( TilesetReader ):
    def __init__( self, *args, **kwargs ):
        super().__init__( *args, **kwargs )
        
        # Determining autotile format (cell and frame number)
        self.nbCellPerCol = int( self.height / TILESIZE)
        self.nbFrames = int(self.width / TILESIZE) if self.nbCellPerCol==1 else int(self.width / (3*TILESIZE))
        self.nbCellPerRow = int(self.width / (self.nbFrames*TILESIZE))
        self.nbCellPerFrame = self.nbCellPerCol * self.nbCellPerRow
        self.size = self.nbCellPerCol * self.nbCellPerRow * self.nbFrames

        self.tiles = { i:dict() for i in range(self.nbFrames) } # dictionary, contains tiles indexed by frameID then tileID
        self.decoder = self.loadDecoder()
        self.subtiles = self.loadSubtiles() # Indexed (by frameID) list of dictionnaries, each containing subtiles indexed by direction and decoder index

        # Check if tileset respects format convention
        assert self.is1x1() or self.is3x4(), \
            f"Format error : expected autotile to be 3x4 or 1x1, found {self.nbCellPerRow}x{self.nbCellPerCol}"
        
    
    def __str__(self):
        return f'Autotile: {self.nbCellPerCol}x{self.nbCellPerRow} cells @{self.nbFrames} frames'


    def loadDecoder( self ):
        data = pd.read_csv( SUBTILE_DECODER, header=0, index_col=0 )
        assert isinstance(data, pd.DataFrame)
        decoder=[None for _ in range(48)]#deque([], maxlen=48)
        for row in data.itertuples():
            cardinalDir = dict()
            cardinalDir['NW'] = row.NW_subtile_Ax
            cardinalDir['NE'] = row.NE_subtile_Bx
            cardinalDir['SE'] = row.SE_subtile_Cx
            cardinalDir['SW'] = row.SW_subtile_Dx
            decoder[row.Index] = cardinalDir
            
        return decoder


    def is3x4(self):
        return (self.nbCellPerCol==4 and self.nbCellPerRow==3)

    def is1x1(self):
        return (self.nbCellPerCol==1 and self.nbCellPerRow==1)

    def loadSubtiles_h( self, frame ):
        """ Helper function.
        Returns all subtiles from a given frame
        Applicable only to 3x4 autotiles.
        
        Each frame of a 3x4 autotile has 12 tiles, but only
        tiles 2-11 are used (0 is redundant and 1 is unused). """

        subtileDBframe = { dir:dict() for dir in SUBTILE_DIRECTIONS }
        for tileID in range(2,12):
            tile = self.getActualTilefromID(tileID, frame)#self.getTilefromID(tileID, frame)
            x = tileID-1 # to fit subtile decoding convention
            for dir in SUBTILE_DIRECTIONS:
                subtile = tile.get_region( *SUBTILE_BOX[dir] )
                subtileDBframe[dir][x] = subtile

        return subtileDBframe

    def loadSubtiles(self):
        if self.is3x4():
            subtileDB = { frame:self.loadSubtiles_h( frame ) for frame in range(self.nbFrames) }
            return subtileDB
        elif self.is1x1():
            return None
        

    # def saveSubtiles(self):
    #     ''' Helper function to insure subtile data is OK by saving each to be observed. '''
    #     for frame, subtileforframe in enumerate(self.subtiles):
    #         #iterate as needed to explore every subtile of each frame
    #         for dir in SUBTILE_DIRECTIONS:
    #             for idx, subtile in enumerate(subtileforframe[dir]):
    #                 if subtile:
    #                     filename=f'SaveSubtiles\\saveSubtiles_frame{frame}_{dir}{idx}.png'
    #                     subtile.save(filename)
    #                 else:
    #                     pass
    #                     #print(f'saveSubtiles: No subtile for frame={frame}, dir={dir}, idx={idx}')

    
        
    def buildTile( self, frame, cardinalDir ):
        ''' Helper function for self.buildTiles
        Builds a tile from 4 subtiles, given a frame ID and cardinal directions IDs
        Returns a tile.
        '''
        #print(f'buildTile: cardinalDir:{type(cardinalDir)}')
        #print(f'buildTile: len(self.subtiles)={len(self.subtiles)}')
        #print(f'buildTile: frame={frame}')
        #print(f'buildTile@{self.imageName}: type(self.subtiles)={type(self.subtiles)}')
        
        newTile = Texture.create(size=(TILESIZE, TILESIZE), colorfmt='rgba')
        
        # Trying fix for resize method
        newTile.mag_filter = 'nearest'

        for direction in SUBTILE_DIRECTIONS:
            tileID = cardinalDir[direction]
            # print(f"frame={frame},tileID={tileID},direction={direction}")
            # print(self.subtiles[0].keys())
            subtile = self.subtiles[frame][direction][tileID]
            newTile.blit_buffer( subtile.pixels, colorfmt='rgba', \
                pos=SUBTILE_BOX[direction][:2], size=SUBTILE_BOX[direction][2:] )
        return newTile
        
    def buildTiles( self, frame ):
        ''' Builds a tile database.
        Each tile corresponds to an entry in the decoder, which maps a tile ID to 
        cardinal direction IDs (values) to select the right subtile to use.
        Returns an indexed tile list.
        '''
        print(f'buildTile@{self._source}, frame {frame}')
        if self.is1x1():
            return self.getTilefromCoordinates( frame, 0 )
        
        # self.is3x4()
        tiles = { idx: self.buildTile( frame, cardinalDir ) \
            for idx, cardinalDir in enumerate(self.decoder) if cardinalDir }
        return tiles

    def getActualTilefromID(self, tileID, frame):
        (row, col) = divmod(tileID, self.nbCellPerRow)
        tile = self.getTilefromCoordinates(col,row + frame * self.nbCellPerRow)
        return tile
    
    def getTilefromID(self, tileID, frame):        
        """ returns the TextureRegion associated with tile ID at given frame.

        :param tileID: tile indexes 
        :param frame: can be larger than nbFrames
        :return: corresponding TextureRegion
        """
        frame = frame % self.nbFrames
        if not self.tiles.get( frame, False ):
            self.tiles[frame] = self.buildTiles(frame=frame)
        
        if self.is3x4():
            return self.tiles[frame][tileID] 
        elif self.is1x1():
            return self.tiles[frame]

class PogerTileset(object):
    """ Represents a Poger-style Tileset

    Provides functionnality to read its content.
    """

    def __init__(self, tileset, resource_manager ):
        self.tileset_name = tileset
        # open the map file
        _tileset = resource_manager.fetch( Resources.TILESET, tileset )
        with _tileset.open(encoding='utf-8-sig') as f:
            try:
                data = json.load(f)
            except Exception as e:
                print(f"Invalid JSON file '{_tileset}' : {e}")
                sys.exit()

            assert data["_class"]=="Tileset", f"Loading {tileset} didn't yield a valid tileset."
            print("fetching tileset_graphic")
            self._tileset_graphic_file = resource_manager.fetch( Resources.GRAPHIC, data["tileset_graphic"] )
            print(f"self._tileset_graphic_file={self._tileset_graphic_file} {type(self._tileset_graphic_file)}")
            self.tileset = PogerTilesetReader( self._tileset_graphic_file )
            self._autotile_files = [ resource_manager.fetch( Resources.GRAPHIC, a ) if a else None for a in data["autotiles"] ]
            self.autotiles = [ PogerAutotileReader( item ) if item else None for item in self._autotile_files ]
            self.passages = data["passages"]
            self.terrain_tags = data["terrain_tags"]
    
    def __str__(self):
        return f'PogerTileset: _tileset_graphic_file={self._tileset_graphic_file}, _autotile_files="{self._autotile_files}"'

    def __repr__(self):
        return '<{0}: "{1}">'.format(self.__class__.__name__, self.tileset_name)

class PogerMap(object):
    """ Represents a Poger-style Map

    Provides functionnality to read the content of a map.
    """

    def __init__(self, map, resource_manager ):
        # open the map file
        _map = resource_manager.fetch( Resources.MAP, map )
        with _map.open(encoding='utf-8-sig') as f:
            try:
                data = json.load(f)
            except Exception:
                print(f"Invalid JSON file '{_map}'")
                sys.exit()

            self.map = np.array(data['table'])
            self.map_name = data["name"]

            self.tileset = PogerTileset( data["tileset"], resource_manager )

        (self.nbLayers, self.height, self.width) = self.map.shape
        self.nbTiles = self.height * self.width
        self.frame = 0
        #self.map_layers = self.sub_map( range(self.width), range(self.height) )

    # def sub_map( self, x_range, y_range ):
    #     positions = [ (x,y) for x in x_range for y in y_range ]
    #     return { 
    #         layer:[ ((x,y),self.map[layer][y][x]) for (x,y) in positions ]
    #         for layer in range(self.nbLayers)
    #     }
    
    def __str__(self):
        return f'PogerMap: map={self.map_name} ({self.map.shape}), tileset="{self.tileset.tileset_name}"'

    def __repr__(self):
        return '<{0}: "{1}">'.format(self.__class__.__name__, self.map_name)
        
    def printMap( self, layer ):
        for y in range(self.height):
            print(self.map[layer][y])

    def getTilefromID_h( self, tileID ):
        ''' Helper function for self.getTilefromID
        From absolute tileID, returns a tuple of the corresponding tileset and relative tileID
        '''
        if not tileID in range(48, TILESET_ZERO + self.tileset.tileset.size):
            #print(f"getTilefromID: tileID out of range : {tileID} !in [48, {TILESET_ZERO+self.tileset.size}).")
            return (None, None)
        if tileID < TILESET_ZERO:
            # tileID refers to an autotile
            (div,actualTileID) = divmod( tileID, 48 )
            autotile_idx = (div)-1
            return (self.tileset.autotiles[autotile_idx], actualTileID)
        else:
            #print(f'tileID={tileID}, Tileset, idx={tileID-TILESET_ZERO}')
            return (self.tileset.tileset, tileID-TILESET_ZERO)

    def getTilefromID(self, tileID):
        (tileset_reader, actualTileID) = self.getTilefromID_h( tileID )
        return tileset_reader.getTilefromID(actualTileID, self.frame) if tileset_reader else None

    def used_tiles( self ):
        return [ i for i in np.unique( self.map ) if TILESET_ZERO <= i ]

    def makePNG_h(self, layer=0, tex=None ):
        if tex is None:
            tex = Texture.create(size=(TILESIZE * self.width, TILESIZE * self.height), colorfmt='rgba')
        
            # Trying fix for resize method
            tex.mag_filter = 'nearest'

        for n in range(self.nbTiles):
            (y,x) = divmod(n, self.width)
            tile_id = self.map[layer][y][x]
            tile = self.getTilefromID( tile_id )
            if tile:
                pos = ( x * TILESIZE, TILESIZE * (self.height-y-1) )
                #print(f"Blitting tile {tile_id} to position {pos}")
                tex.blit_buffer( 
                    tile.pixels, 
                    colorfmt='rgba', 
                    pos=pos,
                    size=(TILESIZE,TILESIZE) )
            #print(f'makePNG_h: n={n}, x={x}, y={y}, tile_id={tile_id}')

        return tex

    def makePNG(self, layer=0 ):
        texture = self.makePNG_h( layer = layer )
        img = Image( texture )
        img.save( f'{self.map_name}_layer{layer}_out.png', flipped=True )

    def makeLayeredPNG( self ):
        tex = Texture.create(size=(TILESIZE * self.width, TILESIZE * self.height), colorfmt='rgba')
        
        # Trying fix for resize method
        tex.mag_filter = 'nearest'

        for i in range(3):
            tex = self.makePNG_h( layer = i, tex=tex )
        img = Image( tex )
        img.save( f'{self.map_name}_layered_out.png', flipped=False )



if __name__=="__main__":
    print("Hello, World!")
    resource_manager = Resources( verbose=True )
    map70 = PogerMap( '*Underwater', resource_manager )
    # print( map70.__str__())
    # print( map70.__repr__())
    # print( map70.tileset.__str__() )
    # print( map70.tileset.__repr__() )
    # for i in range(3):
    #     map70.makePNG( layer = i )
    map70.makeLayeredPNG()
    print("End of program.")