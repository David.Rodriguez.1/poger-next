"""
Author : DavidRodriguezSoaresCUI@github.com

I needed a custom tiled map engine to work with another project of mine
with (unfortunately) exotic tile format.

For the story, I am working on PoGER, a Pokemon Essentials project aiming
at extracting resources and re-implementing it on a open-source engine.

Code was inspired from :
- PyTMX : https://github.com/bitcraft/PyTMX
- kivy-tiled : https://github.com/PhoenixWright/kivy-tiled


MVC:
 - Model : Represents the game world. Receives controller's commands to 
           modify the model, pushes update to view.
 - View : Displays the game to the user. Is re-drawn when model updates it.
 - Controller : polls inputs, issues commands to model.

Note that due to how the model is implemented, even if the game is (for
example) "60 FPS", the display doesn't need to be re-drawn sixty times 
a second, typically because nothing changed from the previous frame.

"""


if __name__=="__main__":
    import kivy
    from kivy.config import Config
    #Config.set('graphics', 'resizable', '0')
    Config.set('kivy', 'log_level', 'debug')

    from pathlib import Path
    import os
    from Map import PogerMap
    from Resources import Resources
    from view import TiledGameDisplay
    from model import GameEngine
    from controller import Controller

    from kivy.core.window import Window
    from kivy.app import App
    from kivy.uix.scatterlayout import ScatterLayout
    from kivy.clock import Clock
    from kivy.logger import Logger
    
    def clear_screen():
        os.system('cls') if os.name == 'nt' else os.system('clear')

    clear_screen()
    resource_manager = Resources( verbose=True )
    map70 = PogerMap( 70, resource_manager )
    view = TiledGameDisplay()
    model = GameEngine( view=view, map=map70, resource_manager=resource_manager )
    controller = Controller( model=model )

    class TiledApp(App):

        def build(self):
            main_widget = ScatterLayout()

            def add_widgets():
                Logger.debug('TiledApp: adding tile map to main widget')
                main_widget.add_widget( view )

            Clock.schedule_once(lambda *args: add_widgets())
            return main_widget

    TiledApp().run()

    print( "Eng of Program")