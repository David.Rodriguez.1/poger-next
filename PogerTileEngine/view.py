import kivy

from kivy.core.window import Window
from kivy.clock import Clock
from kivy.core.image import Image as CoreImage
from kivy.graphics import Color, Rectangle
from kivy.logger import Logger
from kivy.properties import BooleanProperty, ListProperty
from kivy.uix.widget import Widget

from Map import TILESIZE

import random
import time


class TiledGameDisplay(Widget):
    """ The Kivy Widget that implements the View in the MVC design pattern.

    Its purpose is to draw textures/tiles on its canvas to display
    the game world to the user.
    
    Notes:
     - Tile textures are assumed to be square.
    """

    scaled_tile_size = ListProperty()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        self.tiles_on_screen = None
        #self.tile_map_size = None
        self.map_width = self.map_height = None
        self.player_pos = None # equivalent to "camera position"
        self.margin = None
        self._scale = 1.0
        self.tileDB = dict()

        # Window.size = (self.scaled_tile_size[0] * self.tiles_on_screen[0], 
        #                self.scaled_tile_size[1] * self.tiles_on_screen[1])
        #Clock.schedule_interval( self.draw, 1/60 )

    def setup( self, player_pos, tile_size=(32,32), tiles_on_screen=(16,12) ):
        self.tile_size = tile_size
        self.tiles_on_screen = tiles_on_screen
        #self.tile_map_size = tile_map_size
        self.rescale()
        self.margin = ( int( self.tiles_on_screen[0]/2 ), int(self.tiles_on_screen[1]/2) )
        self.player_pos = player_pos
        
    def set_map( self, map_wh ):
        self.map_width, self.map_height = map_wh
    
    @property
    def scale(self):
        return self._scale

    @scale.setter
    def scale(self, value):
        """ Proper way to set display's scale. The screen is not automatically re-drawn.
        """
        self._scale = value
        self.rescale()
        #self.draw()

    def rescale(self):
        """ Computes scaled tile/map dimentions, typically when changing scale.
        """
        self.scaled_tile_size = (self.tile_size[0] * self.scale, self.tile_size[1] * self.scale)
        #self.scaled_map_width = self.scaled_tile_size[0] * self.tile_map_size[0]
        #self.scaled_map_height = self.scaled_tile_size[1] * self.tile_map_size[1]
        self.size = (self.scaled_tile_size[0] * self.tiles_on_screen[0], 
                     self.scaled_tile_size[1] * self.tiles_on_screen[1])

    def draw(self, layers, tiled_map, *args, **kwargs):
        Logger.debug('TileMap: Re-drawing')

        self.player_pos = kwargs.get('player_pos', self.player_pos)

        tick = time.time()

        screen_tile_size = self.get_root_window().width / self.tiles_on_screen[0]
        self.scaled_tile_size = (screen_tile_size, screen_tile_size)
        # self.scaled_map_width = self.scaled_tile_size[0] * self.tile_map_size[0]
        # self.scaled_map_height = self.scaled_tile_size[1] * self.tile_map_size[1]

        # Define the restricted range of tiles that will be in frame
        player_x, player_y = int(self.player_pos[0]), int(self.player_pos[1])

        x_range = range( max( 0, player_x - self.margin[0] ), 
                    min( player_x + self.margin[0] + 1, self.map_width ) )
        y_range = range( max( 0, player_y - self.margin[1] ), 
                    min( player_y + self.margin[1] + 1, self.map_height ) )
        
        self.canvas.clear()
        with self.canvas:
            for layer in layers:

                # set up the opacity of the tiled layer
                Color(1.0, 1.0, 1.0, 1.0)

                # iterate through the tiles in the layer
                for tile_x in x_range:
                    for tile_y in y_range:

                        tileID = layer[tile_y][tile_x]
                        try:
                            if tileID in self.tileDB:
                                texture = self.tileDB[tileID]
                            else:
                                texture = tiled_map.getTilefromID( tileID )
                                #print(f"Fetched tile {tileID}")
                                self.tileDB[tileID] = texture
                        except AttributeError:
                            continue  # keep going if the texture is empty

                        # calculate the drawing parameters of the tile
                        draw_pos = self.get_tile_pos(tile_x, tile_y)

                        # create a rectangle instruction for the gpu
                        try:
                            if texture:
                                Rectangle(texture=texture, pos=draw_pos, size=self.scaled_tile_size )
                                #print(f"Drawn {texture} at {draw_pos}")
                        except TypeError:
                            pass
            Rectangle( pos=self.get_tile_pos(*self.player_pos), size=self.scaled_tile_size )

        tock = time.time()
        print(f"Scene drawn in {tock-tick:.4f}s == {60*(tock-tick):.2f} frames.")


    def get_tile_pos(self, x, _y):
        """Get the tile position relative to the widget."""
        # biased version
        player_x, player_y = self.player_pos[0], self.map_height - self.player_pos[1]
        center_x, center_y = self.tiles_on_screen[0]/2, self.tiles_on_screen[1]/2
        y = self.map_height - _y
        
        pos_x = (x - (player_x - center_x) -0.5) * self.scaled_tile_size[0]
        pos_y = (y - (player_y - center_y) -0.5) * self.scaled_tile_size[1]

        # Original
        # pos_x = x * self.scaled_tile_size[0]
        # pos_y = (self.tile_map_size[1] - y - 1) * self.scaled_tile_size[1]
        return pos_x, pos_y
