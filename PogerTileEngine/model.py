from random import randint

class GameEngine:

    def __init__( self, view, map, resource_manager, *args, **kwargs ):
        super().__init__( *args, **kwargs )

        self.screen = view 
        self.map = map 
        self.rm = resource_manager

        self.player_pos = [37,28]#(45,34)#(0,34)#(45,0)#(0,0)#(37,28)
        self.tile_size = (32,32)

        self.init_screen()


    def init_screen(self):
        cfg = self.rm.get_config()
        self.screen.setup( 
            self.player_pos,
            self.tile_size,
            cfg.tiles_on_screen )

    def move_player( self, dir='random', *args):
        print(f'move_player: dir={dir}')
        val = .5
        if dir=='random':
            self.player_pos[randint(0,1)] += (-1)**(randint(0,1)) * val
        elif dir=='up':
            self.player_pos[1] -= val
        elif dir=='right':
            self.player_pos[0] += val
        elif dir=='down':
            self.player_pos[1] += val
        elif dir=='left':
            self.player_pos[0] -= val
        else:
            return
            
        print(f"Moved player to {self.player_pos}")
        self.screen.draw()